﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="UniLavLIB_DriversAPI" Type="Folder">
			<Item Name="1 UDriver Example.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_DriversAPI/!Builds/UniLavLIB_DriversAPI.llb/1 UDriver Example.vi"/>
			<Item Name="UDriver API.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_DriversAPI/!Builds/UniLavLIB_DriversAPI.llb/UDriver API.vi"/>
			<Item Name="UDriver Get Driver list.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_DriversAPI/!Builds/UniLavLIB_DriversAPI.llb/UDriver Get Driver list.vi"/>
		</Item>
		<Item Name="UniLavLIB_Error_Handler" Type="Folder">
			<Item Name="1 UError Example.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/1 UError Example.vi"/>
			<Item Name="UError apply custom error code.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError apply custom error code.vi"/>
			<Item Name="UError clear ignored code.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError clear ignored code.vi"/>
			<Item Name="UError create delimeters.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError create delimeters.vi"/>
			<Item Name="UError format message.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError format message.vi"/>
			<Item Name="UError init.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError init.vi"/>
			<Item Name="UError Load Custom Error File.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError Load Custom Error File.vi"/>
			<Item Name="UError Save 2 file log.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError Save 2 file log.vi"/>
			<Item Name="UError.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError.vi"/>
		</Item>
		<Item Name="UnitessPluginLink" Type="Folder">
			<Item Name="1 UPL Example plugin.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/1 UPL Example plugin.vi"/>
			<Item Name="2 UPL Example Control.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/2 UPL Example Control.vi"/>
			<Item Name="3 UPL Server Init.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/3 UPL Server Init.vi"/>
			<Item Name="4 UPL Server Cntr Regist.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/4 UPL Server Cntr Regist.vi"/>
			<Item Name="5 UPL Server Start.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/5 UPL Server Start.vi"/>
			<Item Name="6 UPL Server Event DONE.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/6 UPL Server Event DONE.vi"/>
			<Item Name="7 UPL Server STOP.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/7 UPL Server STOP.vi"/>
			<Item Name="Client Connection.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/Client Connection.ctl"/>
			<Item Name="Server Connections.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/Server Connections.ctl"/>
			<Item Name="UPL Client Connect.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Client Connect.vi"/>
			<Item Name="UPL Client Disconnect.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Client Disconnect.vi"/>
			<Item Name="UPL Client Get Control.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Client Get Control.vi"/>
			<Item Name="UPL Client Set Control.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Client Set Control.vi"/>
			<Item Name="UPL Client Sys commands.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Client Sys commands.vi"/>
			<Item Name="UPL find control index.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL find control index.vi"/>
			<Item Name="UPL Get Exe Ver.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Get Exe Ver.vi"/>
			<Item Name="UPL Message Get Parsing.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Message Get Parsing.vi"/>
			<Item Name="UPL Message Set Parsing.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Message Set Parsing.vi"/>
			<Item Name="UPL Server Array pars Boolean.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Array pars Boolean.vi"/>
			<Item Name="UPL Server Array pars Digital.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Array pars Digital.vi"/>
			<Item Name="UPL Server Array pars String.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Array pars String.vi"/>
			<Item Name="UPL Server Avalible Item.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Avalible Item.vi"/>
			<Item Name="UPL Server Cntr Check if already exist.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Cntr Check if already exist.vi"/>
			<Item Name="UPL Server Cntr Regist.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Cntr Regist.vi"/>
			<Item Name="UPL Server Core.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Core.vi"/>
			<Item Name="UPL Server create help Arguments.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server create help Arguments.vi"/>
			<Item Name="UPL Server create help.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server create help.vi"/>
			<Item Name="UPL Server Find Cntr Ref.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Find Cntr Ref.vi"/>
			<Item Name="UPL Server Pars Array.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars Array.vi"/>
			<Item Name="UPL Server Pars BOLLEAN.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars BOLLEAN.vi"/>
			<Item Name="UPL Server Pars ComboBox.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars ComboBox.vi"/>
			<Item Name="UPL Server Pars Delete Quotes.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars Delete Quotes.vi"/>
			<Item Name="UPL Server Pars DIGITAL.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars DIGITAL.vi"/>
			<Item Name="UPL Server Pars ENUM.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars ENUM.vi"/>
			<Item Name="UPL Server Pars Message.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars Message.vi"/>
			<Item Name="UPL Server Pars MulticolumnListBox.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars MulticolumnListBox.vi"/>
			<Item Name="UPL Server Pars RadioButton.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars RadioButton.vi"/>
			<Item Name="UPL Server Pars RING.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars RING.vi"/>
			<Item Name="UPL Server Pars SLIDE id21.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars SLIDE id21.vi"/>
			<Item Name="UPL Server Pars TabControl.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars TabControl.vi"/>
			<Item Name="UPL Server Pars TIMESTAMP.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars TIMESTAMP.vi"/>
			<Item Name="UPL Server Select Item.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Select Item.vi"/>
			<Item Name="UPL Server value 2 str array.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str array.vi"/>
			<Item Name="UPL Server value 2 str combox.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str combox.vi"/>
			<Item Name="UPL Server value 2 str digit.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str digit.vi"/>
			<Item Name="UPL Server value 2 str enum.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str enum.vi"/>
			<Item Name="UPL Server value 2 str mclb.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str mclb.vi"/>
			<Item Name="UPL Server value 2 str ring.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str ring.vi"/>
			<Item Name="UPL Server value 2 str tab.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str tab.vi"/>
			<Item Name="UPL Server VI Control Get.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server VI Control Get.vi"/>
			<Item Name="UPL Server VI Control Set.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server VI Control Set.vi"/>
			<Item Name="UPL Server VI Sys comand.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server VI Sys comand.vi"/>
			<Item Name="UPL TCP Multiple Connections Data.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP Multiple Connections Data.ctl"/>
			<Item Name="UPL TCP read Error.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP read Error.vi"/>
			<Item Name="UPL TCP read Event Flag.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP read Event Flag.vi"/>
			<Item Name="UPL TCP read Wait Event Done.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP read Wait Event Done.vi"/>
			<Item Name="UPL TCP read.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP read.vi"/>
			<Item Name="UPL TCP Write Error.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP Write Error.vi"/>
			<Item Name="UPL TCP Write Event Done.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP Write Event Done.vi"/>
			<Item Name="UPL TCP Write Event Flag.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP Write Event Flag.vi"/>
			<Item Name="UPL TCP write.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP write.vi"/>
			<Item Name="UPL_Cntr Regist Info.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_Cntr Regist Info.ctl"/>
			<Item Name="UPL_Control.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_Control.ctl"/>
			<Item Name="UPL_Controls.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_Controls.ctl"/>
			<Item Name="upl_core_stm.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/upl_core_stm.ctl"/>
			<Item Name="UPL_delimeter_data.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_delimeter_data.vi"/>
			<Item Name="UPL_Events_Type.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_Events_Type.ctl"/>
			<Item Name="UPL_Exp2_Postfix.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_Exp2_Postfix.vi"/>
			<Item Name="UPL_Postfix Parser.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_Postfix Parser.vi"/>
			<Item Name="UPL_RunFile.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_RunFile.vi"/>
			<Item Name="UPL_StateMachine.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_StateMachine.ctl"/>
		</Item>
		<Item Name="UniLavLIB_Menu" Type="Folder">
			<Item Name="1 UMenu Example.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/1 UMenu Example.vi"/>
			<Item Name="UMenu Back.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/UMenu Back.vi"/>
			<Item Name="UMenu Engineeriong menu password.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/UMenu Engineeriong menu password.vi"/>
			<Item Name="UMenu Init.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/UMenu Init.vi"/>
			<Item Name="UMenu Serf.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/UMenu Serf.vi"/>
			<Item Name="UMenu Settings.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/UMenu Settings.ctl"/>
			<Item Name="UMenu tab ref.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/UMenu tab ref.ctl"/>
		</Item>
		<Item Name="UniLavLIB_ControlsUI" Type="Folder">
			<Item Name="1_CUI_Example.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/1_CUI_Example.vi"/>
			<Item Name="Button.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/Button.ctl"/>
			<Item Name="CUI Action Open FP.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI Action Open FP.vi"/>
			<Item Name="CUI Action.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI Action.vi"/>
			<Item Name="CUI Control sett.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI Control sett.ctl"/>
			<Item Name="CUI Controls sett.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI Controls sett.ctl"/>
			<Item Name="CUI Extract settings from desc.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI Extract settings from desc.vi"/>
			<Item Name="CUI GUI Silver.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI GUI Silver.vi"/>
			<Item Name="CUI GUI Unitess Black.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI GUI Unitess Black.vi"/>
			<Item Name="CUI GUI Unitess Blue steel.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI GUI Unitess Blue steel.vi"/>
			<Item Name="CUI UI Style.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI UI Style.ctl"/>
			<Item Name="String.ctl" Type="VI" URL="../../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/String.ctl"/>
		</Item>
		<Item Name="UniLavLIB_Useful" Type="Folder">
			<Item Name="Convert Panel to Screen Coordinate.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Convert Panel to Screen Coordinate.vi"/>
			<Item Name="Create folder if not exist.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Create folder if not exist.vi"/>
			<Item Name="CSV load.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/CSV load.vi"/>
			<Item Name="Excel export ArrayOfstr.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Excel export ArrayOfstr.vi"/>
			<Item Name="Excel open.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Excel open.vi"/>
			<Item Name="File Dialog.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/File Dialog.vi"/>
			<Item Name="Get All Controls Ref From VI.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Get All Controls Ref From VI.vi"/>
			<Item Name="Get All inserted Controls Ref From Control.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Get All inserted Controls Ref From Control.vi"/>
			<Item Name="Get EXE name.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Get EXE name.vi"/>
			<Item Name="Get Exe Ver.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Get Exe Ver.vi"/>
			<Item Name="Get monitor resolution.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Get monitor resolution.vi"/>
			<Item Name="INI_SaveLoad.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/INI_SaveLoad.vi"/>
			<Item Name="IS_EXE.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/IS_EXE.vi"/>
			<Item Name="Make Top VI Frontmost.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Make Top VI Frontmost.vi"/>
			<Item Name="Make VI Frontmost.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Make VI Frontmost.vi"/>
			<Item Name="MCL or Tbl 2 Excel.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/MCL or Tbl 2 Excel.vi"/>
			<Item Name="Postfix2expV2_point.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Postfix2expV2_point.vi"/>
			<Item Name="Postfix_ENG_2_RUS.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Postfix_ENG_2_RUS.vi"/>
			<Item Name="RemoveBadSymbols (SubVI).vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/RemoveBadSymbols (SubVI).vi"/>
			<Item Name="RunDifferentFile.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/RunDifferentFile.vi"/>
			<Item Name="RunVI.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/RunVI.vi"/>
			<Item Name="SaveFileAsSTR.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/SaveFileAsSTR.vi"/>
			<Item Name="Set VI screen coordinates.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Set VI screen coordinates.vi"/>
			<Item Name="Set Window Z-Position (hwnd).vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Set Window Z-Position (hwnd).vi"/>
			<Item Name="Startup Add EXE to it.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Startup Add EXE to it.vi"/>
			<Item Name="Startup Delete EXE from it.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Startup Delete EXE from it.vi"/>
			<Item Name="Startup Is EXE in it.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Startup Is EXE in it.vi"/>
			<Item Name="StopOrExit (SubVI).vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/StopOrExit (SubVI).vi"/>
			<Item Name="STR_translit.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/STR_translit.vi"/>
			<Item Name="Unitess_Format_Value.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Unitess_Format_Value.vi"/>
			<Item Name="Variable_SaveLoad.vi" Type="VI" URL="../../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Variable_SaveLoad.vi"/>
		</Item>
		<Item Name="DenisLIB_HID" Type="Folder">
			<Item Name="HID.vi" Type="VI" URL="../../../DenisLIB/DenidLIB_HID/!Builds/DenisLIB_HID.llb/HID.vi"/>
			<Item Name="HID_connect.vi" Type="VI" URL="../../../DenisLIB/DenidLIB_HID/!Builds/DenisLIB_HID.llb/HID_connect.vi"/>
			<Item Name="HID_disconnect.vi" Type="VI" URL="../../../DenisLIB/DenidLIB_HID/!Builds/DenisLIB_HID.llb/HID_disconnect.vi"/>
			<Item Name="HID_example.vi" Type="VI" URL="../../../DenisLIB/DenidLIB_HID/!Builds/DenisLIB_HID.llb/HID_example.vi"/>
			<Item Name="HID_read.vi" Type="VI" URL="../../../DenisLIB/DenidLIB_HID/!Builds/DenisLIB_HID.llb/HID_read.vi"/>
			<Item Name="HID_request.vi" Type="VI" URL="../../../DenisLIB/DenidLIB_HID/!Builds/DenisLIB_HID.llb/HID_request.vi"/>
			<Item Name="HID_write.vi" Type="VI" URL="../../../DenisLIB/DenidLIB_HID/!Builds/DenisLIB_HID.llb/HID_write.vi"/>
		</Item>
		<Item Name="Lib" Type="Folder" URL="../Lib">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Immunity EMC Plugin.vi" Type="VI" URL="../Immunity EMC Plugin.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_Gmath.lvlib" Type="Library" URL="/&lt;vilib&gt;/gmath/NI_Gmath.lvlib"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Write Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value Simple.vi"/>
				<Item Name="Write Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value Simple STR.vi"/>
				<Item Name="Write Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value.vi"/>
				<Item Name="Write Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value STR.vi"/>
				<Item Name="Write Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value DWORD.vi"/>
				<Item Name="Write Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value Simple U32.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Delete Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Delete Registry Value.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="Set Enum String Value__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Enum String Value__ogtk.vi"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Get Strings from Enum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum__ogtk.vi"/>
				<Item Name="Get Strings from Enum TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum TD__ogtk.vi"/>
				<Item Name="Current VIs Parents Ref__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/Current VIs Parents Ref__ogtk.vi"/>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="mcHID.dll" Type="Document" URL="../../../DenisLIB/builds/DenisLIB_HID/data/mcHID.dll"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="ImmunityEMCPlugin" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{8F1378A4-7F6A-431B-BFA2-9785D5386B2E}</Property>
				<Property Name="App_INI_GUID" Type="Str">{4A8599C7-0211-457E-8AA0-D7238FA4B327}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{65815629-2042-4BBC-A9CC-D25E2E96BD50}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">ImmunityEMCPlugin</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{E6383FA3-13FC-4FC5-A6ED-2D03E62C5800}</Property>
				<Property Name="Bld_version.build" Type="Int">36</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">ImmunityEMCPlugin.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/ImmunityEMCPlugin.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{9A1BA8C7-28AE-4011-8DA7-F01C8304B79B}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Immunity EMC Plugin.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_fileDescription" Type="Str">ImmunityEMCPlugin</Property>
				<Property Name="TgtF_internalName" Type="Str">ImmunityEMCPlugin</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017 </Property>
				<Property Name="TgtF_productName" Type="Str">ImmunityEMCPlugin</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{19758BD0-589A-41E3-BDFF-719117CD97D9}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">ImmunityEMCPlugin.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
